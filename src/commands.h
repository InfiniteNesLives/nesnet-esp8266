
#ifndef _commands_h
#define _commands_h


//bit7: W/R 1-write 0-read
#define WR_CMD	0x80
//bit6: quick/immediate==1 access 2x64Bytes of vars/regs
#define VARIABLE_CMD	0x40
//bit5: long/rainbow==1 next byte contains length
#define MED_LEN_MASK  0x0F   //0 is currently invalid length
//	med==0 length in lower nibble
#define RAINBOW_CMD 0x20
#define RAINBOW_CONN_MASK  0x0F   //lower nibble stores connection number to send message
//bit4: special==1 command in lower nibble
//	the lengths are pre-defined
#define SPECIAL_CMD 0x10
//#define SPECIAL_OPERANDS 0x20 //operands associated with special opcode
#define FAST_CMD   0x08 //special command that gets reply in ISR instead of main thread
//#define SLOW_CMD   0x00 //special command that gets reply in ISR instead of main thread
#define SPECIAL_CMD_MASK  0x0F

//special commands (0-15)
//could expand to have 15 indicate that next byte is the command, IDK how many we'll want..
//SLOW COMMANDS
#define SCMD_RESET 0	//NO OPERANDS, SLOW_CMD
	#define RESET_VAL 0xA5 //expected reply that the ESP was reset
#define SCMD_MARK_READ 1//NO OPERANDS, SLOW
//#define SCMD_MOD_WIFI  2+SPECIAL_OPERANDS//SLOW
//#define SCMD_MOD_CONN  3+SPECIAL_OPERANDS//SLOW
//FAST COMMANDS
#define SCMD_MSG_POLL 1+FAST_CMD
#define SCMD_MSG_SENT 2+FAST_CMD
	//reply is number of messages in the incoming buffer

#define UNDEF_CMD 0x100 //can't fit in byte


#endif
