
#ifndef _interrupt_h
#define _interrupt_h

#include <Arduino.h>
#include "commands.h"
#include "buffer.h"
#include "io_esp01_bbangspi.h"

#define OPCODE 0
#define DATA 1

ICACHE_RAM_ATTR void incoming_byte_isr();
int special_cmd(uint8_t cur_command);

#endif
