
//#ifdef ESP01_BBANGSPI

#include "io_esp01_bbangspi.h"





//original bad because MISO has slow rising edge, have to wait too long to sample SPI
//int spi_clk = 0;		//HIGH to boot from SPI
//int spi_mosi = 1;  //LED ESP-01   (slow rising edge input)
//int spi_miso = 2;  //LED ESP-01S  (slow rising edge input) HIGH boot from SPI
//int spi_int = 3;

int spi_miso = 0;		//HIGH to boot from SPI
int spi_int = 1;  //LED ESP-01   (slow rising edge input)
int spi_ctl = 1;  //LED ESP-01   (slow rising edge input)
#define LED 2 //active low
#define CTL_PARALLEL HIGH
#define CTL_SERIAL LOW
int spi_clk = 2;  //LED ESP-01S  (slow rising edge input) HIGH boot from SPI
int spi_mosi = 3;
#define SPI_REG_LEN 10  //8bit data + 2bit Status

void io_init() {
  //pinMode(0, INPUT); //clk
  //pinMode(1, INPUT); //MOSI

  pinMode(spi_clk, OUTPUT); //clk
  pinMode(spi_mosi, OUTPUT); //MOSI
  pinMode(spi_int, OUTPUT); //INT

  //pinMode(0, OUTPUT); //clk
  //pinMode(1, OUTPUT); //MOSI

  //pinMode(2, INPUT); //MISO
  pinMode(spi_miso, INPUT); //MISO
  //pinMode(3, INPUT); //INT

  //digitalWrite(0, LOW);   // turn the LED on (HIGH is the voltage level)
  //digitalWrite(1, LOW);   // turn the LED on (HIGH is the voltage level)
//  digitalWrite(spi_clk, LOW);   // turn the LED on (HIGH is the voltage level)
//  digitalWrite(spi_mosi, LOW);   // turn the LED on (HIGH is the voltage level)
//  digitalWrite(spi_int, HIGH);   // turn the LED on (HIGH is the voltage level)

  //flush SPI reg with 0xFF and set W bit so 6502 can perform first write
  init_spi_reg();

  //attachInterrupt(digitalPinToInterrupt(spi_miso), miso_isr, CHANGE);
  attachInterrupt(digitalPinToInterrupt(spi_miso), incoming_byte_isr, FALLING);
}

void init_spi_reg() {
  //initialize mapper spi register to 0xFF with W set
	digitalWrite(spi_int, LOW);  //serial 
	int i;

	digitalWrite(spi_mosi, HIGH); //SPI reg input 
  //TODO define SR length
	for ( i=0; i<10; i++) {
		//data = data<<1;

		digitalWrite(spi_clk, HIGH);  
		digitalWrite(spi_clk, LOW);  
		//sample miso
  }

	digitalWrite(spi_int, HIGH);   //parallel

}

int fetch_spi_byte() {
	//bit bang out data via SPI

	digitalWrite(spi_ctl, CTL_SERIAL);   //Set the SPI register in serial mode

	//TODO can speed up this code by trashing status bits without polling and shifting data
	
	//set the MOSI pin to shift 1's into SPI register and set W/R bits to clear isr
	digitalWrite(spi_mosi, HIGH);

	int i;
	int data=0;
	for ( i=0; i<SPI_REG_LEN; i++) { //Will cause us to drop the first Status bits & set them to 1

		data = data>>1; //LSB first, shift to the right

		digitalWrite(spi_clk, HIGH);  //indicate to logic analyzer we're about to poll MISO

		//sample miso
		if(digitalRead(spi_miso) == HIGH) {
    		//	Udp.write("1");
			data +=0x80; //shifting bits in from the left
		}
	//	else {
    	//		Udp.write("0");
	//	}	 //45.7usec using UDP write for each bit first clk rise to last fall
		//28.4usec when just storing byte
    //24.6usec arduinoIDE CPU-80Mhz flash-40Mhz
    //12.8usec platformIO CPU-160Mhz flash-80Mhz = 23 cycles on 6502
    //7 cycles on 6502 per BIT-Branch ~= 3 BIT-BR loops + time for ESP to service ISR

		//shift out next bit
		digitalWrite(spi_clk, LOW);
	}	

	digitalWrite(spi_ctl, CTL_PARALLEL);  //Set SPI reg to parallel so 6502 can write again

  return data;
}

//void send_spi_byte(int data) {
void send_spi_byte(int data) {
	//bit bang data into SPI reg

	digitalWrite(spi_ctl, CTL_SERIAL);   //Set the SPI register in serial mode

	//need to set W & clear R bits
	//W=1 tells 6502 it can write to $5000 if desired (for next command)
	//R=0 tells 6502 our reply is in $5000 register
	data = data << 2; //lower 2 bits clear
	data += 1; //W set

	int i;
	for ( i=0; i<SPI_REG_LEN; i++) { //Will cause us to drop the first Status bits & set them to 1

		digitalWrite(spi_clk, HIGH);  //prepare to latch next bit & indicate bit about to change

		if(data & 0x01) {
			//bit set
			digitalWrite(spi_mosi, HIGH);
		} else {
			//bit clear
			digitalWrite(spi_mosi, LOW);
		}

		data = data>>1; //LSB first, shift to the right

		//latch the bit
		digitalWrite(spi_clk, LOW);
	}	

	digitalWrite(spi_ctl, CTL_PARALLEL);  //Set SPI reg to parallel so 6502 can write again

}


/* OLD no longer used...
void send_gpio_udp() {
    //Udp.beginPacket(SERVER_IP, conn0_port);
    Udp.beginPacket(conn0_ip, conn0_port);

    	Udp.write("GPIO READ 3210=0b");

	if(digitalRead(3) == HIGH) //spi_int
    		Udp.write("1");
	else
    		Udp.write("0");

	if(digitalRead(2) == HIGH) //spi_miso
    		Udp.write("1");
	else
    		Udp.write("0");

	if(digitalRead(1) == HIGH) //spi_mosi
    		Udp.write("1");
	else
    		Udp.write("0");

	if(digitalRead(0) == HIGH) //spi_clk
    		Udp.write("1");
	else
    		Udp.write("0");

	//bit bang out data via SPI

	//serial mode
	//Udp.write(" INT_LO");
	digitalWrite(spi_int, LOW);   // turn the LED on (HIGH is the voltage level)

	//Udp.write(" b0-");

	int i;
	int data=0;
  //TODO define SPI_REG_LEN
	for ( i=0; i<SPI_REG_LEN; i++) { //Will cause us to drop the first Status bits & set them to 1
		//data = data<<1;
		data = data>>1; //LSB first, shift to the right

		digitalWrite(spi_clk, HIGH);  
		//sample miso
		if(digitalRead(spi_miso) == HIGH) {
    		//	Udp.write("1");
			data +=0x80; //shifting bits in from the left
		}
	//	else {
    	//		Udp.write("0");
	//	}	 //45.7usec using UDP write for each bit first clk rise to last fall
		//28.4usec when just storing byte
    //24.6usec arduinoIDE CPU-80Mhz flash-40Mhz
    //12.8usec platformIO CPU-160Mhz flash-80Mhz = 23 cycles on 6502
    //7 cycles on 6502 per BIT-Branch ~= 3 BIT-BR loops + time for ESP to service ISR

		//shift out next bit
		digitalWrite(spi_clk, LOW);
	}	

	Udp.write("SPI READ=");
	char data_array[3] = "0x";
	Udp.write(data_array);

	String byte_string = String(data, HEX);
	byte_string.toCharArray(data_array, 3);
	Udp.write(data_array);

	//back to parallel mode
	//serial mode
	Udp.write(" INT_HI");
	digitalWrite(spi_int, HIGH);   // turn the LED on (HIGH is the voltage level)


    Udp.endPacket();
}
*/

//#endif //ESP01_BBANGSPI
