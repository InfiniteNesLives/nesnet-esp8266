
#include "interrupt.h"
#include "buffer.h"

extern int outgoing[VAR_ARRAY_SIZE]; //local 6502 writes to these, need to send to remote ESP
extern int incoming[VAR_ARRAY_SIZE]; //local 6502 reads from these, need to update when remote ESP sends
extern uint8_t msg_buff[1024]; //messages from the internet 1KB buffer 0x0000 - 0x03FF (4pages)
extern char data_main[256];
extern uint8_t *msg_ptr;// = msg_buff;

extern int update_flag;// = 0;
extern int pending_out;// = 0;

int data_idx = 0;

int command = UNDEF_CMD;

int command_mode = OPCODE;

ICACHE_RAM_ATTR void incoming_byte_isr() {
	//TODO disable interrupts
	//apparently this is already happening..?
	//when MISO falls while shifting data, this ISR doesn't get re-triggered..

	//measured 2.2-2.4usec from mosi falling edge to next instruction (spi_clk going high)
	//digitalWrite(spi_clk, HIGH);  //logic analyzer sees when ISR starts

	//fetch command byte & clear interrupt
	int data = fetch_spi_byte();

	//process command byte
	//int reply = data^0xFF;

	int temp;

	//what mode are we in?
	if (command_mode == OPCODE) {
		command = data;

		if (command & VARIABLE_CMD) {
			if (command & WR_CMD) {
				//WRITE set mode and wait for data bytes
				command_mode = DATA;
			} else {
				//READ don't need data, just fetch reply
				temp = command & VAR_NUM_MASK;
				//reply = incoming[temp];
				send_spi_byte(incoming[temp]);
			}
		}
		else if (command & SPECIAL_CMD) {
			//command in nibble, or defined length
			//command_mode = special_cmd(command);
			//STAY IN COMMAND MODE
			special_cmd(command);

		} else { //message mode
			if (command & WR_CMD) {
				//not variable index addressing
				//assume single byte command for now
				command = data;
				command_mode = DATA;
				if (command & RAINBOW_CMD) {
					data_main[0] = (command & RAINBOW_CONN_MASK);
					data_idx = 1;
				} else { //medium mode length in lower nibble of command
					data_main[0] = 0; //always connection #0
					data_main[1] = (command & MED_LEN_MASK);
					data_idx = 2;
				}
			} else {
				//READ mode, fetch next byte
				send_spi_byte(*msg_ptr);
				msg_ptr++; //inc ptr for next read
				//send_spi_byte(msg_buff[idx]);
			}
		}
	} else { //DATA mode
		if ((command & (VARIABLE_CMD+WR_CMD))==(VARIABLE_CMD+WR_CMD)) {
			//WRITE variable
			temp = command & VAR_NUM_MASK;
			outgoing[temp] = data;
			//incoming[temp] = data; //TODO remove for testing only
			//got all the data
			command_mode = OPCODE;
			//inform main a variable was updated so it can be sent
			update_flag = 1;
		//{ else if (command & SPECIAL_CMD) {
		//	//more data for the command
		//	command_mode = special_data(data);
		} else {
			//WRITE opcodes bytes that follow coming from SPI reg-6502
			if (command & WR_CMD) {
				//not variable mode, have main process command
				data_main[data_idx] = data;
				//long mode, data_main[0] == 
				//if (command & RAINBOW_CMD) {
					if (data_idx == (data_main[1]+1)) { //+1 for connection number
						//received all incoming data
						data_main[data_idx+1] = 0; //null terminate TODO remove
						command_mode = OPCODE;
						//command_flag = 1;
						//TODO just increment this
						pending_out = 1;
					}
				//} else {
				//	//medium mode, len in opcode lower nibble (1-15 is valid)
				//	//TODO use zero length for special case?  Currently invalid
				//	if (data_idx == (command&MED_LEN_MASK)) {
				//	//if (data_idx == command) {
				//		//received all incoming data
				//		data_main[data_idx] = 0; //null terminate TODO remove
				//		command_mode = OPCODE;
				//		//command_flag = 1;
				//		pending_out = 1;
				//	}
				//}
				data_idx++;
			} else {
			//READ opcodes, put data in SPI reg for the 6502
			//Done up above...
			}
		}
	}
	
	
	//if 6502 expecting reply, send it
	//send_spi_byte(reply);
 
	//debug UDP message
	//print_udp_byte(data);
 
}

extern uint8_t pending_msgs;// = 0;
extern int main_cmd;// = 0;
extern int main_cmd_flag;// = 0;

int special_cmd(uint8_t cur_command) {
	
	int next_mode = OPCODE;

	//Fast commands have bit 3 set
	if (cur_command & FAST_CMD) {

		switch(cur_command&SPECIAL_CMD_MASK) {
			case SCMD_MSG_POLL:
				send_spi_byte(pending_msgs);
				break;
			case SCMD_MSG_SENT:
				send_spi_byte(pending_out);
				break;
		}
		
	} else { //slow command
	//	if (cur_command & SPECIAL_OPERANDS) {
	//		//need more data to process command
	//		next_mode = DATA;
	//		special_idx = 0;
	//	} else {
			//no operands, ready for main to process command
			main_cmd = cur_command & SPECIAL_CMD_MASK;
			main_cmd_flag = 1;
	//	}
	}

	return next_mode;
}

/*
int special_data(uint8_t cur_data) {

	int next_mode = DATA;

	//next byte of data
	special_data[special_idx] = cur_data;
	special_idx++;

	switch (command) {
	       
		case SCMD_MOD_CONN:
			next_mode = DATA;
			break;		
		case SCMD_MOD_WIFI:
			next_mode = DATA;
			break;		
	
	}

	return next_mode;
}
*/

