
#ifndef _buffer_h
#define _buffer_h

#include <Arduino.h>

//variable arrays
#define VAR_ARRAY_SIZE 64 //max of addressing mode b7 WR, b6=VARMODE
#define VAR_NUM_MASK 0x3F //64 addresses b5-0

#endif
