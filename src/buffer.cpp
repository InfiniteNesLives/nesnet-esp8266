
#include "buffer.h"

int outgoing[VAR_ARRAY_SIZE]; //local 6502 writes to these, need to send to remote ESP
int incoming[VAR_ARRAY_SIZE]; //local 6502 reads from these, need to update when remote ESP sends

char data_main[256];	//messages from the NES

uint8_t msg_buff[1024]; //messages from the internet 1KB buffer 0x0000 - 0x03FF (4pages)
uint8_t *msg_ptr = msg_buff;
