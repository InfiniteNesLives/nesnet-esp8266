
/*
 * NESnet GPIO main module
*/

#include <Arduino.h>

////ESP-01S 1MByte flash, NO power LED, built-in LED Blue GPIO2 (also connected to 8pin)
////BUILT IN LED: GPIO2 INVERTED (blue), no power LED
//int pin = 2; //pinned out to 8pin (also ESP-01S only blue LED)

//void setup() {
//  // initialize GPIO 2 as an output.
//  pinMode(pin, OUTPUT);
//}

// the loop function runs over and over again forever
//void loop() {
//  pinMode(pin, OUTPUT);
//  digitalWrite(pin, HIGH);   // turn the LED on (HIGH is the voltage level)
//  delay(3000);               // wait for a second
//
//  digitalWrite(pin, LOW);    // turn the LED off by making the voltage LOW
//  delay(3000);               // wait for a second
//}


#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

#define ESP01_BBANGSPI
//#define ESP01_CICOP_UART


#ifdef ESP01_BBANGSPI
#include "io_esp01_bbangspi.h"
#endif

#include "interrupt.h"
#include "commands.h"
#include "buffer.h"

#ifndef STASSID
#define STASSID "NESnet_guest"
#define STAPSK  "infinite"
#endif


#define SERVER_IP "192.168.1.250" //desktop
//#define SERVER_IP "192.168.1.251" //Rpi
char conn0_ip[64] = SERVER_IP;

//unsigned int conn0_port = 1250;      // local port to listen on
unsigned int conn0_port = 1234;      // local port to listen on

//#define SET_UDP		0x00
//#define SET_TCP		0x01
uint8_t conn0_protocol = 0; //SET_UDP


// buffers for receiving and sending data
char packetBuffer[UDP_TX_PACKET_MAX_SIZE + 1]; //buffer to hold incoming packet,
char  ReplyBuffer[] = "acknowledged\r\n";       // a string to send back

extern uint8_t *msg_ptr;//= msg_buff;
extern uint8_t msg_buff[1024]; //messages from the internet 1KB buffer 0x0000 - 0x03FF (4pages)
extern char data_main[256];
extern int outgoing[VAR_ARRAY_SIZE]; //local 6502 writes to these, need to send to remote ESP
extern int incoming[VAR_ARRAY_SIZE]; //local 6502 reads from these, need to update when remote ESP sends

int update_flag = 0;
//int command_flag = 0;
int pending_out = 0;

int main_cmd = 0;
int main_cmd_flag = 0;

WiFiUDP Udp;

//pass a string to send UDP packet
void send_udp(char* message) {
    //Udp.beginPacket(SERVER_IP, conn0_port);
    Udp.beginPacket(conn0_ip, conn0_port);
    Udp.write(message);
    Udp.endPacket();
}


// Use WiFiClient class to create TCP connections
WiFiClient client;

void tcp_connect() {
  //client.connect(conn0_ip, conn0_port);
//    Serial.println("connection failed");
//    delay(5000);
//    return;
  //if (!client.connect(host, port)) {
  if (!client.connect(conn0_ip, conn0_port)) {
    //Serial.println("connection failed");
  	send_udp("TCP connection failed..");
    //delay(5000);
    //return;
  } else {
  	send_udp("TCP connection success!");

  	if (client.connected()) {
  	  client.println("TCP connected, test message");
  	} else {
  		send_udp("wasn't connected when testin before message");
  	  client.println("TCP not connected");
	}
  }

  return;
}

//sends C formatted string followed by a carriage return and newline
void send_tcp_string(char* message) {
  if (client.connected()) {
    //client.println("hello from ESP8266");
    client.println(message);
  }
}

//void send_tcp_binary(uint8_t* data, int len) {
void send_tcp_binary(char* data, int len) {
	int i;
	if (client.connected()) {
	//send 1 byte at a time because that's how the library works...
	
		for (i=0; i<len; i++) {
		       // client.write(data[i]);
			client.print( data[i], HEX);
		}
	}
}


void print_udp_byte(int data) {
	Udp.beginPacket(conn0_ip, conn0_port);
 
	Udp.write("DATA BYTE= 0x");
	char data_array[3];// = "0x";
	//Udp.write(data_array);

	String byte_string = String(data, HEX);
	byte_string.toCharArray(data_array, 3);
	Udp.write(data_array);

	Udp.endPacket();
}

void print_udp_array(int *data, int len) {

	char data_str[3];// = "0x";

	Udp.beginPacket(conn0_ip, conn0_port);

	Udp.write("DATA ARRAY hi->lo:");

	len--; //offset to index of array

	while (len >= 0) {
		Udp.write(" ");

		String byte_string = String(data[len], HEX);
		byte_string.toCharArray(data_str, 3);
		Udp.write(data_str);

		len--;
	}

	Udp.endPacket();
}

uint8_t pending_msgs = 0;
//uint8_t pending_out = 0;


uint8_t special_data[16];
uint8_t special_idx = 0;


/*
  test (shell/netcat):
  --------------------
	  nc -u 192.168.esp.address 8888
then type the message to send...

The first time, netcat didn't get an acknowledge reply, but it did after second message
*/

void setup() {
#ifdef SERIAL_DBG
  Serial.begin(115200);
#endif
  WiFi.mode(WIFI_STA);
  WiFi.begin(STASSID, STAPSK);
  while (WiFi.status() != WL_CONNECTED) {
#ifdef SERIAL_DBG
    Serial.print('.');
#endif
    delay(500);
  }
#ifdef SERIAL_DBG
  Serial.print("Connected! IP address: ");
  Serial.println(WiFi.localIP());
  Serial.printf("UDP server on port %d\n", conn0_port);
#endif
  Udp.begin(conn0_port);

//let's send a UDP packet to initialize connection with socketsv.py
    //Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
    //Udp.beginPacket("192.168.0.100", 1234);
 //   Udp.beginPacket(SERVER_IP, conn0_port);
	Udp.beginPacket(conn0_ip, conn0_port);
    Udp.write("first UDP message opening 'connection' with socketsv.py");
    Udp.endPacket();

#ifdef SERIAL_DBG
  send_udp("SERIAL ENABLED");
#else
  send_udp("SERIAL DISABLED");
#endif

	//GPIO init
	io_init();


  send_udp("CLK MOSI output low, others input mode");

  send_udp("MISO pin interrupts enabled");

	//initialize data array & test debug
//	int i;	
//	for ( i=0; i<64; i++) {
//		incoming[i] = (i<<2);	
//		outgoing[i] = i+1;
//		update_flag = 1;
//	}

	//test TCP connection
	//figure out why this stopped working...
	//tcp_connect();
	//send_tcp("test tcp message");

}

void reset() {

	int i;
	for ( i=0; i<VAR_ARRAY_SIZE; i++) {
		incoming[i] = 0;
		outgoing[i] = 0;
	}

	//TODO what other things should be cleared/reset..?
	
	//indicate to 6502 that we accepted reset command
	send_spi_byte(RESET_VAL);
}

#define MODIFY_CONN 0x10
	#define MODIFY_CONN_IP		0x00
	#define MODIFY_CONN_PORT	0x01
	#define MODIFY_CONN_PROTOCOL	0x02
		#define SET_UDP		0x00
		#define SET_TCP		0x01
void update_metadata( uint8_t meta_cmd, char *data ){

	switch (meta_cmd) {

		case MODIFY_CONN:
			switch ( data[0] ) {
				//value starting in data[1]
				case MODIFY_CONN_IP:
					strcpy(conn0_ip, &data[1]);
					break;
				case MODIFY_CONN_PORT:
					conn0_port = data[1] | (data[2]<<8);
					break;
				case MODIFY_CONN_PROTOCOL:
					if ( data[1] == SET_TCP ) {
						//establish connection if not already made
  						//if (!client.connected()) {
  						//	tcp_connect();
  						//} 
						//assume IP address may have changed and always make new connection
  						tcp_connect();
					}
					conn0_protocol = data[1];
					break;
			}
			break;

	}

	return;
}

#define WR_VAR_ALL "WVA " //next 64Bytes are all variables
#define WR_VAR_NUM "WVN " //next byte is var number, then value
#define HEADERLEN 4 
#define NEWLINE 1

void loop() {

	////turn on LED
  	//pinMode(0, OUTPUT); //clk
  	//pinMode(1, OUTPUT); //clk
  	//pinMode(2, OUTPUT); //clk
  	//pinMode(3, OUTPUT); //clk
	////digitalWrite(LED, LOW);   // turn the LED on 
	//digitalWrite(0, LOW);   // turn the LED on 
	//digitalWrite(1, LOW);   // turn the LED on 
	//digitalWrite(2, LOW);   // turn the LED on 
	//digitalWrite(3, LOW);   // turn the LED on 

	//digitalWrite(0, HIGH);   // turn the LED on 
	//digitalWrite(1, HIGH);   // turn the LED on 
	//digitalWrite(2, HIGH);   // turn the LED on 
	//digitalWrite(3, HIGH);   // turn the LED on 
	//return;


	// if there's data available, read a packet
	int i;
	int packetSize = Udp.parsePacket();
	if (packetSize) {
#ifdef SERIAL_DBG
	Serial.printf("Received packet of size %d from %s:%d\n    (to %s:%d, free heap = %d B)\n",
		packetSize,
		Udp.remoteIP().toString().c_str(), Udp.remotePort(),
		Udp.destinationIP().toString().c_str(), Udp.conn0_port(),
		ESP.getFreeHeap());
#endif
	
	// read the packet into packetBufffer
	int n = Udp.read(packetBuffer, UDP_TX_PACKET_MAX_SIZE);
	packetBuffer[n] = 0;

#ifdef SERIAL_DBG
	Serial.println("Contents:");
	Serial.println(packetBuffer);
#endif

	//ESP received a packet from remote console/server
	//interpret the header included in the UDP/TCP packet payload
	if (packetBuffer[0]=='W' & packetBuffer[1]=='V' & packetBuffer[2]=='A') {
		//update all variables
  		//send_udp("WVA command received");
		for( i=0; i<(packetSize-HEADERLEN-NEWLINE); i++) {
			incoming[i] = packetBuffer[i+HEADERLEN];
		}


	} else if (packetBuffer[0]=='W' & packetBuffer[1]=='V' & packetBuffer[2]=='N') {
		//next byte gives variable number, then value
  		//send_udp("WVN command received");
		//send udp 0 WVN 0A   <-  sets variable zero to ascii A
		incoming[packetBuffer[HEADERLEN]-0x30] = packetBuffer[HEADERLEN+1]; //convert from ascii to hex

	} else if (packetBuffer[0]=='M'){

		//copy data into buffer
		for( i=0; i<(packetSize-HEADERLEN-NEWLINE); i++) {
			msg_buff[i] = (uint8_t)packetBuffer[i+HEADERLEN];
		}
		//reset pointer to begining
		msg_ptr = msg_buff;
		//increment message count
		pending_msgs++;

	} else {
		//doesn't match
  		send_udp("unknown command received:");
  		send_udp(packetBuffer);
	}

//	incoming[0] = packetBuffer[0];
//	incoming[1] = packetBuffer[1];
//	incoming[2] = packetBuffer[2];
//	incoming[3] = packetBuffer[3];
//	
//	incoming[4] = packetBuffer[4];
//	incoming[5] = packetBuffer[5];
//	incoming[6] = packetBuffer[4]-0x30;

	//debug print statement of current variables
//	print_udp_array(incoming, VAR_ARRAY_SIZE);
//
//	// send a reply, to the IP address and port that sent us the packet we received
//	Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
//	Udp.write(ReplyBuffer);
//	Udp.endPacket();
  } //end of incoming packet

	//check if the 6502 updated any of it's variables and transmit
	if (update_flag) {
		//for now, let's just transmit all variables
		print_udp_array(incoming, VAR_ARRAY_SIZE);
		print_udp_array(outgoing, VAR_ARRAY_SIZE);
		update_flag = 0;
	}

	//if (command_flag) {
	if (pending_out) {

	  	//send_udp(&data_main[2]);

		//process message 
		//Byte0: command/conn#
		//Byte1: length
		//Bytes2+ data/message
		//command_flag = 0;

		switch (data_main[0]) {

			case 0: //send to conn# 0
	  		//	send_udp(data_main);
				if (conn0_protocol == SET_UDP) {
	  				send_udp(&data_main[2]);
				} else {
	  				//send_tcp_string(&data_main[2]);
					//send_tcp_binary((uint8_t*)&data_main[2],  data_main[1]);
	  				//send_tcp_string(&data_main[2]);
	  				//send_udp(&data_main[2]);

					/*
	  				send_tcp_string(&data_main[2]);
					//<RP>

					send_tcp_binary(&data_main[2],  data_main[1]);
	  				send_tcp_string(&data_main[2]);
					//<525000RP>
					
					client.write(   (uint8_t*)&data_main[2],  data_main[1]);
	  				//send_tcp_string(&data_main[2]);
					//<RP
					//	1:      0x52
					//	2:      0x50
					//	3:      0x0
					//	4:      0x0
					//	5:      0x52
					//	6:      0x50
					*/

					client.write(   (uint8_t*)&data_main[2],  data_main[1]);
					//client.write_P(   (char*)data_main[2],  data_main[1]);
					//seems that write pushes data to outgoing buffer
					//but packet doesn't get sent till print/println is called
    					//client.flush();
    					//client.print("\n");
					//produces desired result:
					//len:    4       <RP  >
					//1:      0x52
					//2:      0x50
					//3:      0x0
					//4:      0x0
					//TODO IDK how sending binary version of newline works though...
					//reqpage:
					//  .byte 4
					//  .byte "RP" ;request page
					//  ;.word $0000 ;page num
					//  ;.word $1A25 ;page num (assembler sets little endian as needed)
					//  .byte $0D ;CR
					//  .byte $0A ;LF
					//seems it doesn't work...
					//len:    2       <RP>
					//1:      0x52
					//2:      0x50
					//3:
					//4:
					//seems arduino client.write is pretty worthless for binary data..
					//must be just trying convert to ascii 
					//which means binary CR/LF get translated to send  *facepalm*
					//https://github.com/esp8266/Arduino/blob/master/doc/esp8266wifi/client-class.rst
				}
				break;

			case 0xF: //meta data
				update_metadata( data_main[2], &data_main[3] );
				break;

		}
		//TODO just decrement this
		pending_out = 0;
	}

	if (main_cmd_flag) {
		switch(main_cmd) {
			case SCMD_RESET: 
				reset(); 
				break;
			case SCMD_MARK_READ: 
				if (pending_msgs > 0) {
					pending_msgs--;
				}
				break;
			default:
				; //compiler requires a statement..?
		}
		//command processed, can accept new command
		main_cmd_flag = 0;
	}

//send every second
//  delay(3000);               // wait for a second
//  send_gpio_udp();

}

