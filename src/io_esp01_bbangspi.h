#ifndef _io_esp01_bbangspi_h
#define _io_esp01_bbangspi_h

#include <Arduino.h>

#include "interrupt.h"

//comment out define to disable serial
//#define SERIAL_DBG

void io_init();

int fetch_spi_byte();
void send_spi_byte(int data);
void init_spi_reg();

#endif
